import { Component, OnInit ,Inject } from '@angular/core';
import { TrackingComponent } from '@app/modules/tracking/tracking.component';
import { ContactComponent } from '@app/modules/contact/contact.component';
import pathData from '@app/shared/routes/path/path.json';
import { MenusService } from '@app/shared/services/menus.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {

  public MenusObj;
  
  constructor(private menuService : MenusService) { 
  }

  ngOnInit()
   {

     this.MenusObj = this.menuService.renderMenus();

    }


}
