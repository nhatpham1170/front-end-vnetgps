import { Component, OnInit } from '@angular/core';
import { ThemeService } from '@app/core/services/theme.service';
import { DefaultService } from '@app/shared/services/default.service';

@Component({
  selector: 'app-color-themes',
  templateUrl: './color-themes.component.html',
  styleUrls: ['./color-themes.component.scss']
})
export class ColorThemesComponent implements OnInit {


  public themeDefault = 'navy-aside';
  public listColorThemes;
  constructor(

    private themeService : ThemeService,
    private defaultService : DefaultService

  ) {

    if(this.defaultService.getThemeDefault())
    {
      this.themeDefault =  this.defaultService.getThemeDefault();
    }
    this.themeService.loadColorTheme(this.themeDefault);
    this.listColorThemes = this.themeService.getListTheme();
   }

  ngOnInit() {
  }
  changeColorTheme(name){
    this.themeService.loadColorTheme(name);
  }
}
