import { Component, OnInit } from '@angular/core';
import pathData from '@app/shared/routes/path/path.json';

@Component({
  selector: 'app-user-bar',
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.scss']
})
export class UserBarComponent implements OnInit {
  public username;
  public name;
  public pathLogin;
  constructor() { }

  ngOnInit() {

    this.pathLogin = '/'+pathData.auth.logout.path;
    if(localStorage.getItem('dataRoot') != null)
    {
      const data    = JSON.parse(localStorage.getItem('dataRoot'));
      this.username = data.user.username;
      this.name     = data.user.name;
    }

  }

}
