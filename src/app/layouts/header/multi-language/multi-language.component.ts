import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-multi-language',
  templateUrl: './multi-language.component.html',
  styleUrls: ['./multi-language.component.scss']
})
export class MultiLanguageComponent implements OnInit {
  public languageList;
  public selectLanguage;
  public langDefault = 'vi';
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'vi']);
    translate.setDefaultLang(this.langDefault);

    this.languageList = [
      { lang: 'vi', label: 'Vietnamese', icon : './assets/media/flags/vietnam.jpg' },
      { lang: 'en', label: 'English', icon : './assets/media/flags/020-flag.svg' }
    ];
    this.selectLanguageDefault(this.langDefault);

  }

  ngOnInit() {
    
  }
  changeLanguge(lang)
  {
    this.translate.use(lang);
    this.selectLanguageDefault(lang);
    console.log(this.selectLanguage);
  
  }
  selectLanguageDefault(lang)
  {
    for (let i = 0; i < this.languageList.length; i++) {
      if(this.languageList[i]['lang'] == lang){
        this.selectLanguage = [this.languageList[i]];
        break;
      }
    }
  }
}
