import { NgModule } from '@angular/core';

// import { CommonModule }  from '@angular/common';
// import { FormsModule } from '@angular/forms';
// shared
import { SharedModule } from '@app/shared';
import { SidebarComponent } from '@app/layouts/sidebar/sidebar.component';

// routing
import { ThemeRoutingModule } from './theme-routing.module';

// component
import { TrackingComponent } from './tracking/tracking.component';
import { ContactComponent } from './contact/contact.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { GeofenceComponent } from './geofence/geofence.component';
import { ManageUsersComponent } from './manage/manage-users/manage-users.component';


@NgModule({
	declarations: [
		TrackingComponent,
		ContactComponent,
		GeofenceComponent,
		ManageUsersComponent
	],
	imports: [
		// CommonModule,
		// FormsModule,
		SharedModule,
		ThemeRoutingModule,
		AngularFontAwesomeModule
	]
})
export class ThemeModule { }
