import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManageUsersService {
  public endpoint = 'http://admin.haduwaco.com/';

  constructor(
    private http: HttpClient,
    ) { 
  
  }

  private extractData(res: Response) {
    ;

    let body = res;
    return body || { };
  }
  getProducts(): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Access-Control-Allow-Origin','*');
    headers = headers.append('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, PATCH, DELETE');
    headers = headers.append('Access-Control-Allow-Headers', 'Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
    return this.http.get('http://admin.haduwaco.com/api/users/userTreeLower', {headers})
  }
}
