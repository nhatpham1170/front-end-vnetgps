import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { ManageUsersService } from './manage-users.service';


@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {
 public products;
  constructor(
    private router : Router,
    private http: HttpClient,
    private manageUser: ManageUsersService
    ) { }

  ngOnInit() {
    this.getProducts();
  } 
  getProducts() {
    
    this.products = [];
    this.manageUser.getProducts().subscribe((data: {}) => {
      console.log(data);
      this.products = data;
    });
  }
}
