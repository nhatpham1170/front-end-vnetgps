import { NgModule } from '@angular/core';

// import { CommonModule }  from '@angular/common';
// import { FormsModule } from '@angular/forms';
// shared
import { SharedModule } from '@app/shared';

// routing
import { AuthRoutingModule } from './auth-routing.module';

// component
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
	declarations: [
		LoginComponent,
		
	],
	imports: [
		// CommonModule,
		// FormsModule,
		SharedModule,
		AuthRoutingModule,
		AngularFontAwesomeModule
	]
})
export class AuthModule { }
