import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, NgForm,Validators  } from '@angular/forms';
import { AuthService } from '@app/core/services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MenusService } from '@app/shared/services/menus.service';
import { TokenService } from '@app/shared/services/token.service';
import { DefaultService } from '@app/shared/services/default.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  isLoginError: Boolean = false;
  loginForm: FormGroup;
  isLoading: boolean;
  
  public logo;
  public background;
  public tracking;
  
  constructor(
    private userService:AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private menuService : MenusService,
    private tokenService:TokenService,
    private defaultService : DefaultService
     ){ 
      this.buildForm();
     }

  ngOnInit() {

    this.logo = this.defaultService.getLogo();
    this.background = this.defaultService.getBackground();

    localStorage.clear();
    this.tracking = this.menuService.getPathById('tracking');
    if(localStorage.getItem('userToken') != null)
    {
      this.router.navigate([this.menuService.getPathById('tracking')]);
    }

  }
	get f() {
		return this.loginForm.controls;
	}

  onSubmit(form:any){  
    // console.log(form.value);
    if (this.loginForm.invalid) {
      return;
    }
    this.userService.userLogin(form.value.username,form.value.password)
    .subscribe((data:any)=>{
        if(data.status==200) {
          this.isLoading = true;
          const token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBbGV4MTIzIiwic2NvcGVzIjoiUk9MRV9kZXZpY2UuYWN0aW9uLmNvbW1hbmQsUk9MRV9kZXZpY2UuYWN0aW9uLmVkaXQsUk9MRV9kZXZpY2UuYWN0aW9uLmltcG9ydF9kZXZpY2UsUk9MRV9kZXZpY2UuYWN0aW9uLmxvY2F0aW9uLFJPTEVfZGV2aWNlLmFjdGlvbi5wbGF5YmFjayxST0xFX2RldmljZS5hY3Rpb24ucmVuZXdlZCxST0xFX2RldmljZS5hY3Rpb24uc2VsbCxST0xFX2RldmljZS5lZGl0LmFjdGl2ZV9jdXN0b21lcixST0xFX2RldmljZS5lZGl0LmFjdGl2ZV9zdGF0dXMsUk9MRV9kZXZpY2UuZWRpdC5hZGRfZ3JvdXAsUk9MRV9kZXZpY2UuZWRpdC5kZXNjcmlwdGlvbl9hZG1pbixST0xFX2RldmljZS5lZGl0Lmdyb3VwLFJPTEVfZGV2aWNlLmVkaXQuaWNvbixST0xFX2RldmljZS5lZGl0LmluYWN0aXZlX2N1c3RvbWVyLFJPTEVfZGV2aWNlLmVkaXQudHlwZSxST0xFX2RldmljZS5tYW5hZ2UsUk9MRV9nZW9mZW5jZS5tYW5hZ2UsUk9MRV9sb2dpblBhZ2UubWFuYWdlLFJPTEVfcGVybWlzc2lvbi5tYW5hZ2UsUk9MRV9wcm9maWxlLmFueS52aWV3LFJPTEVfcHJvZmlsZS5vd24udmlldyxST0xFX3Byb2ZpbGUub3duLnZpZXcyLFJPTEVfcm9sZS5tYW5hZ2UsUk9MRV91c2VyLmxpc3QiLCJpYXQiOjE1NjE1MzY1NTgsImV4cCI6MTU2MTU1NDU1OH0.bP4ijAORtOl8A9XLPZzAP_hWYmrBMlwvQ7gN8EcQ49Q";
          //decode toke
          this.tokenService.token = token;
          let listRoles = this.tokenService.getListRoles();
          //end list roles
          //set localstorage
          localStorage.setItem('userRoles',JSON.stringify(listRoles));          
          localStorage.setItem('userToken',token);          
          localStorage.setItem('tokenExpire',data.tokenExpire);
          localStorage.setItem('dataRoot',JSON.stringify(data));
          //end set localstorage
          this.router.navigate([this.menuService.getPathById('geofence')]);
        }
        else{
          this.isLoginError=true;
        }                        
      },
      (err:HttpErrorResponse)=>{
        this.isLoginError=true;
      });    
  }

  private buildForm(): void {
		this.loginForm = this.formBuilder.group({
			username: ['', Validators.required],
			password: ['', Validators.required]
		});
	}
}
