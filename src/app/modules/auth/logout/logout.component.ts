import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/core/services/auth.service';
import { Router } from '@angular/router';
import pathData from '@app/shared/routes/path/path.json';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private userService : AuthService,private router : Router) { }

  ngOnInit() {

    localStorage.clear();
    this.logout();

  }

  logout(){
     this.userService.userLogout();
     this.router.navigate([pathData.auth.login]);

  }
}
