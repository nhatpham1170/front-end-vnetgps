import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrackingComponent } from './tracking/tracking.component';
import { ContactComponent } from './contact/contact.component';
import { GeofenceComponent } from './geofence/geofence.component';
import { ManageUsersComponent } from './manage/manage-users/manage-users.component';

import { AuthGuard } from '@app/core';

const routes: Routes = [

	{
		path: '',
		children: [
			{
				path: 'tracking',
				component : TrackingComponent,
				data : {id:'device'},
				canActivate: [AuthGuard]

			},
			{
				
				path: 'playback',
				component : ContactComponent,
				data : {id:'playback'},
				canActivate: [AuthGuard]

			},
			{
				
				path: 'geofence',
				component : GeofenceComponent,
				data : {id:'geofence'},
				canActivate: [AuthGuard]

			},
			{
				
				path: 'manage-users',
				component : ManageUsersComponent,
				data : {id:'geofence'},
				canActivate: [AuthGuard]

			}

		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ThemeRoutingModule { }
