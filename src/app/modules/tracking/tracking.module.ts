import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { TrackingRoutingModule } from './tracking-routing.module';
import { TrackingComponent } from './tracking.component';


@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    TrackingRoutingModule,
    SharedModule
  ]
})
export class TrackingModule { }
 