import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';
import { Employee } from '../shared/employee.model';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  list:Employee[];
  sub:Subscription;
  constructor(private service:EmployeeService,
    private toastr:ToastrService) { }

  ngOnInit() {  
    this.service.getEmployees().subscribe(data=>{
      this.list = data.map(item=>{
        return {
            id:item.payload.doc.id,
            ...item.payload.doc.data()        
        } as Employee;
      })
    })
  }
  onEdit(emp:Employee){   
    this.service.formData = Object.assign({},emp);
  }
  onDelete(id:string){
    this.service.deleteEmployee(id);
    this.toastr.warning('Deleted employee',"EMP");
  }

}
