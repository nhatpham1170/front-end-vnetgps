import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
// import 'rxjs/add/operator/do';
// import { Router } from '@angular/router';
// import { environment } from '@env/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor{
    constructor(){
    }
    intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>{
      // add authorization header with jwt token if available
      let userToken = localStorage.getItem('userToken');
      if (userToken) {
        req = req.clone({
              setHeaders: {
                  Authorization: `Bearer ${userToken}`
              }
          });
      }
      return next.handle(req);
  }
}

