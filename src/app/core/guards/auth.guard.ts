import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot,CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import pathData from '@app/shared/routes/path/path.json';
import { MenusService } from '@app/shared/services/menus.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(
		private router:Router,
		private menusService:MenusService

		) {}

	canActivate(
		route: ActivatedRouteSnapshot, state: RouterStateSnapshot
	): boolean {   

		// check token
		if(localStorage.getItem('userToken') === null)
		{
			 this.router.navigate([pathData.auth.login.path]);	
			 return false;

		}

		//check tokenExpire
		let tokenExpire= new Date(localStorage.getItem('tokenExpire'));
		let dateNow = new Date();   
		console.log("now : "+dateNow+ " tokenexpire"+ tokenExpire);
   
		if(dateNow>tokenExpire){
			 this.router.navigate([pathData.auth.login.path]);	
			 return false;
		}

		//check role user
		let id = route.data.id;
		let userRoles = localStorage.getItem('userRoles');
		let role = this.menusService.getRoleById(id);

		if(!userRoles.includes(role))
		{
			this.router.navigate([this.menusService.getPathById('geofence')]);	
			return false;

		}

		return true;
	} 
}
