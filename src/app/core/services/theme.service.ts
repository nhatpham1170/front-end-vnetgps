import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ThemeService {
	public head;
	public listColorThemes = [
		{'name':'navy-aside','label':'Navy Aside','link':[
		  {'id':'header-light','url':'assets/css/demo1/skins/header/base/light.css'},
		  {'id':'brand-navy','url':'assets/css/demo1/skins/brand/navy.css'},
		  {'id':'aside-navy','url':'assets/css/demo1/skins/aside/navy.css'}
		]},
		{'name':'light-aside','label':'Light Aside','link':[
		  {'id':'header-light','url':'assets/css/demo1/skins/header/base/light.css'},
		  {'id':'brand-light','url':'assets/css/demo1/skins/brand/light.css'},
		  {'id':'aside-light','url':'assets/css/demo1/skins/aside/light.css'}
		]},
		{'name':'navy-header','label':'Navy Header','link':[
		  {'id':'header-navy','url':'assets/css/demo1/skins/header/base/navy.css'},
		  {'id':'brand-navy','url':'assets/css/demo1/skins/brand/navy.css'},
		  {'id':'aside-light','url':'assets/css/demo1/skins/aside/light.css'}
		  
		]}
	  ];
	  
    public listLink = [
    {'id':'brand-navy','url':'assets/css/demo1/skins/brand/navy.css'},
    {'id':'aside-navy','url':'assets/css/demo1/skins/aside/navy.css'},
    {'id':'brand-light','url':'assets/css/demo1/skins/brand/light.css'},
    {'id':'aside-light','url':'assets/css/demo1/skins/aside/light.css'},
    {'id':'header-light','url':'assets/css/demo1/skins/header/base/light.css'},
    {'id':'header-navy','url':'assets/css/demo1/skins/header/base/navy.css'}
    ];
	constructor() 
	{
		this.head  = document.getElementsByTagName('head')[0];
	}
	getListTheme()
	 {
		 return this.listColorThemes;
	 }
	loadColorTheme(name){
		this.removeAllLink();
		for (let i = 0; i < this.listColorThemes.length; i++)
		{
		   let itemListColor = this.listColorThemes[i];
		   if(itemListColor['name'] == name)
		   {
			 let listLink = itemListColor['link'];
			 for (var j = 0; j < listLink.length; j++)
			 {
			  this.loadLinkCss(listLink[j]['url'],listLink[j]['id']);
			 }
		   }
		}
	
	  }
	removeLinkCss(idLink)
	 {
	   if(document.getElementById(idLink))
	   {
		 let ele = document.getElementById(idLink);
		 this.head.removeChild(ele);
	   }
	 }
	loadLinkCss(linkCss,idLink)
	 {
	   if (!document.getElementById(idLink))
	   {
		 let link  = document.createElement('link');
		 link.id   = idLink;
		 link.rel  = 'stylesheet';
		 link.type = 'text/css';
		 link.href = linkCss;
		 link.media = 'all';
		 this.head.appendChild(link);
	   }	
	 }
	removeAllLink()
	 {
	   for (let i = 0; i < this.listLink.length; i++)
	   {
		 this.removeLinkCss(this.listLink[i]['id']);
	   }
	 }

}
