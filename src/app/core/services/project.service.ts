import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { JsonApiService } from './json-api.service';

// model
import { Project } from '../models/project.class';

const routes = {
	projects: '/projects',
	project: (id: number) => `/projects/${id}`
};

@Injectable({
	providedIn: 'root'
})
export class ProjectService {

	constructor(
		private jsonApiService: JsonApiService,
		private apiService: ApiService) 
	{ }

	getAll(): Observable<Project[]> {
		//console.log(routes.projects);
		return this.apiService.get(routes.projects);
	}

	getSingle(id: number): Observable<Project> {
		return this.apiService.get(routes.project(id));
	}
}
