import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // readonly rootUrl = 'http://api.vitrioto.com:8000';
  constructor(private http: HttpClient) { }

  userLogin(userName,password){

    let reqHeader=new HttpHeaders()
    .set('AppID',environment.api.AppID)
    .set('Accept','application/json')
    .set('Content-Type','application/json');

    return this.http.post(environment.api.host+'/login',{'username':userName,'password':password},{headers:reqHeader});
  } 

  userLogout(){
      localStorage.clear();
      localStorage.removeItem('userToken');
  }

}
