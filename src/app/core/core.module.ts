import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// guards
import { AuthGuard } from './guards/auth.guard';
import { NoAuthGuard } from './guards/no-auth.guard';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';

//
// import { NgxSpinnerModule } from 'ngx-spinner';
import { TokenInterceptor } from './interceptors/token.interceptor';

@NgModule({
	declarations: [

	],
	imports: [
		HttpClientModule,
		// NgxSpinnerModule
	],
	providers: [
		AuthGuard,
		NoAuthGuard,
		{
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
	]
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}
}
