import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//component
import { ThemeComponent } from './theme/dashboard-theme/theme.component';
import { AuthThemeComponent } from './theme/auth-theme/auth-theme.component';
import { LogoutComponent } from '@app/modules/auth/logout/logout.component';
import { ContactComponent } from '@app/modules/contact/contact.component';

import { AuthGuard } from '@app/core';

import pathData from '@app/shared/routes/path/path.json';
 
const routes: Routes = [
	{
		path: '',
		redirectTo: pathData.auth.login.path,
		pathMatch: 'full'
	},

	{
		path: '',
		component: ThemeComponent,
		loadChildren: '@app/modules/theme.module#ThemeModule',

	},

	{
		path: 'auth',
		component: AuthThemeComponent,
		loadChildren: '@app/modules/auth/auth.module#AuthModule'
	},
	{
		path: '**',
		redirectTo: pathData.auth.login.path,
		pathMatch: 'full'
	},
	{
		path: pathData.auth.logout.path,
		component : LogoutComponent
	},

	



];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
