import  { Routes } from '@angular/router';
import { MenusService } from '@app/shared/services/menus.service';


export const CONTENT_ROUTES: Routes = [
  {
    path: 'tracking',
    loadChildren: '@app/modules/tracking/tracking.module#TrackingModule'
  },
  // {
  //   path: 'about',
  //   loadChildren: './modules/about/about.module#AboutModule'
  // },
  {
    path: 'playback',
    loadChildren: '@app/modules/contact/contact.module#ContactModule',
    data: {role: 'Admin'}
  }
];
