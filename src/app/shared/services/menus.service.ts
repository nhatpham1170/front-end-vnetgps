import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import pathData from '@app/shared/routes/path/path.json';
import { empty } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class MenusService {

    public path;
    public role;
    
    constructor() {
        this.path = 'path';
        this.role = 'role';
    }

    public getPathById(id)
    {
        return this.getItem(id,this.path);
    }

    public getRoleById(id)
    {
        return this.getItem(id,this.role);
    }
    public getItem(id,item){
        let menus = pathData.menu;
        for (let i = 0; i < menus.length; i++)
        {
            let itemMenus = menus[i]['item_menus'];
            if(itemMenus.length > 0)
            {
                for (let j = 0; j < itemMenus.length; j++)
                {
                    if(itemMenus[j]['id'] == id)
                    {
                        return itemMenus[j][item]; 
                    }
                }
            }

        }
    }
    public renderMenus()
    {
        
      let listRoles = localStorage.getItem('userRoles');
      let menus = pathData.menu;
      for (let i = 0; i < menus.length; i++)
      {
        let items = menus[i]['item_menus'];
        
        for (let j = 0; j < items.length; j++)
        {
            let role = items[j]['role'];
            if(!listRoles.includes(role))
            {
    -         items.splice(j,1); 
            //  console.log("remove: " +j+" role: "+role);
                j = j-1;
            }
        }
      }
      return menus;
    }
}
