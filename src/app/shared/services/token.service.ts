import { Injectable } from '@angular/core';
import pathData from '@app/shared/routes/path/path.json';
import { empty } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
	providedIn: 'root'
})
export class TokenService {
    public helper ;
    public token;
    public decodedToken;
    constructor() {
        this.helper = new JwtHelperService();
    }
    ngOnInit() 
    {
    }

    public setToken(token)
    {
        this.token = token;
    }
    public getToken()
    {
        return this.token;
    }
    public decodeToken()
    {
        this.decodedToken = this.helper.decodeToken(this.token);
        return this.decodedToken;
    }

    public getListRoles()
    {

        let scopes = this.decodeToken().scopes;
        let arrayScopes   = scopes.split(",");
        let listRoles= [];
        //get list roles
        for (var i = 0; i < arrayScopes.length; i++)
        {
          var start = 5;
          var end = arrayScopes[i].indexOf(".");
          var string = arrayScopes[i].substring(start,end);
          if(!listRoles.includes(string)){
            listRoles.push(string);
          }
        }

        return listRoles;
    }



}
