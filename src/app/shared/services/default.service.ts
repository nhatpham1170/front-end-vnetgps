import { Injectable } from '@angular/core';
import loginPath  from '@app/shared/routes/load-domain/default.json';

declare var document: any;

@Injectable()
export class DefaultService {

  public logo;
  public background;
  public elementId = 'favicon';
  public link_favicon;
  public theme_default;
  public obj_domain;

  constructor() {   
    this.init();
    this.removeFavicon();  
    this.setFavicon(this.link_favicon);
  }

  init() {   
    var base_url = location.origin; 
    var foundDomain = this.founDomain(base_url);
    if(foundDomain)
    {
        var ele  =  this.obj_domain;
        var login = ele.login;
        this.link_favicon  = login.favicon;
        this.logo          = login.logo;
        this.theme_default = ele.theme_default;
        this.background    = {
            background: 'url('+login.background+')'
           };
    }
  }

  getLogo()
  {
    return this.logo;
  }
  
  getBackground()
  {
    return this.background;
  }

  getThemeDefault()
  {
    return this.theme_default;
  }
  
  setFavicon(link_favicon){

    var linkElement = document.createElement( "link" );
    linkElement.setAttribute( "id", this.elementId );
    linkElement.setAttribute( "rel", "icon" );
    linkElement.setAttribute( "href", link_favicon );
    document.head.appendChild( linkElement );

  }

  removeFavicon()
  {
    var linkElement = document.head.querySelector( "#" + this.elementId );
    if ( linkElement ) {
    document.head.removeChild( linkElement );
    }
  }
  
  founDomain(domain)
  {
    
    for (var i = 0 ; i < loginPath.length ; i++)
      {

        var domainInPath = loginPath[i].domain;
        if(domainInPath == domain)
        {
          console.log("domain: "+domain);
          this.obj_domain = loginPath[i];
          return true;

        }
        this.obj_domain = loginPath[loginPath.length-1];
        return true;
    }
  }

}