import { Injectable } from '@angular/core';
import { Employee } from './employee.model';
import { NgForm } from '@angular/forms';
import { AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  formData: Employee;
  collection:AngularFirestoreCollection;
  constructor(private fireStore:AngularFirestore) {
    this.collection = fireStore.collection('employee');
    this.resetForm();
   }
  resetForm(form? : NgForm){
    if(form!=null)
      form.resetForm();   
    this.formData = {
      id:null,
      fullName:'',
      position:'',
      empCode:'',
      mobile:'',
    }
  }
  addEmployee(data:Employee){
    this.collection.add(data);
  }
  updateEmployee(data:Employee){    
    this.collection.doc(data.id).update(data);
  }
  deleteEmployee(id:string){
    this.collection.doc(id).delete();
  }
  getEmployees(){
    return this.collection.snapshotChanges();
  }
}
