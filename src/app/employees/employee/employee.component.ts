import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '@app/shared/employee.service';
import { NgForm } from '@angular/forms';
import { Toast, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  constructor(private service: EmployeeService,
    private toastr:ToastrService) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm){
    let data = Object.assign({},form.value) ;
    // delete data.id;
    if(form.value.id==null){
      delete data.id;
      this.service.addEmployee(data);
      this.toastr.success('Add employee succesfully','EMP. Register');
    }
    else{
      this.service.updateEmployee(data);
      this.toastr.success('Update employee succesfully','EMP. Register');
    }
   
    this.service.resetForm(form);
    
  }
}
