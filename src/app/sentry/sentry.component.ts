import { Component, OnInit } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { environment } from '@env/environment';

@Component({
  selector: 'app-sentry',
  templateUrl: './sentry.component.html',
  styleUrls: ['./sentry.component.scss']
})
export class SentryComponent implements OnInit {

  value:string=environment.sentry.dsn;
  constructor() { 
    // this.value.abc=2;
  }

  ngOnInit() {   
   
  }

}
