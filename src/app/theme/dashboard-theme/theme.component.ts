import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamicScriptLoader.service';
import { DefaultService } from '@app/shared/services/default.service';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.scss']
})
export class ThemeComponent implements OnInit {
  constructor(
    private defaultService: DefaultService,
    private dynamicScriptLoader: DynamicScriptLoaderService
    ) {
   }

  ngOnInit() {

  }
}
