import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable, ErrorHandler } from '@angular/core';
import * as Sentry from '@sentry/browser';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule } from '@angular/forms';
// import { AngularFireModule } from '@angular/fire/database';

//import { CoreModule } from '@app/core';
import { SharedModule } from './shared';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SentryComponent } from './sentry/sentry.component';
import { environment } from '../environments/environment';
import { from } from 'rxjs';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeComponent } from './employees/employee/employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';

import { EmployeeService } from './shared/employee.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import { ThemeComponent } from './theme/dashboard-theme/theme.component';
import { SidebarComponent } from '@app/layouts/sidebar/sidebar.component';
import { HeaderComponent } from './layouts/header/header.component';
import { ContentComponent } from './layouts/content/content.component';
import { TrackingComponent } from './modules/tracking/tracking.component';
import { ContactComponent } from './modules/contact/contact.component';
import { ColorThemesComponent } from './layouts/header/color-themes/color-themes.component';
import { LogoutComponent } from '@app/modules/auth/logout/logout.component';

import localeVi from '@angular/common/locales/vi';
import { MultiLanguageComponent } from './layouts/header/multi-language/multi-language.component';
import { MenusComponent } from './layouts/sidebar/menus/menus.component';
import { BreadcrumbComponent } from './layouts/breadcrumb/breadcrumb.component';
import { AuthModule } from './modules/auth/auth.module';
import { AuthThemeComponent } from './theme/auth-theme/auth-theme.component';
import { UserBarComponent } from './layouts/header/user-bar/user-bar.component';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamicScriptLoader.service';
import { DefaultService } from '@app/shared/services/default.service';


Sentry.init({
  dsn: environment.sentry.dsn
});
@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    SentryComponent,
    EmployeesComponent,
    EmployeeComponent,
    EmployeeListComponent,
    ThemeComponent,
    HeaderComponent,
    ContentComponent,
    ColorThemesComponent,
    MultiLanguageComponent,
    MenusComponent,
    BreadcrumbComponent,
    AuthThemeComponent,
    UserBarComponent, 
    LogoutComponent,
    SidebarComponent
  ],
  imports: [
  // core & share
    //CoreModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    { 
      provide: ErrorHandler, 
      useClass: SentryErrorHandler 
    },
    EmployeeService,
    DynamicScriptLoaderService,
    DefaultService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
