// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  sentry:{ 
    dsn:"http://fb3c7b419e484193a521e88f826a2d34@sentry.vnetgps.com:9000/24",
  },
  firebaseConfig:{
    apiKey: "AIzaSyAv44uFNKzHw-DCMVviDrX8gq35tgnP7q0",
    authDomain: "fir-demo-4f15f.firebaseapp.com",
    databaseURL: "https://fir-demo-4f15f.firebaseio.com",
    projectId: "fir-demo-4f15f",
    storageBucket: "fir-demo-4f15f.appspot.com",
    messagingSenderId: "624876828608",
    appId: "1:624876828608:web:26b35795a1b321a4", 
    
  },
  api:{
    host:'http://api.vitrioto.com:8000',
    AppID:'Mr2n4FaCO8XdS7K6x4sHbIT6L+Gumltq7dy/EW0eIXQ=',
  },
  osrm:{
    host:'http://125.212.203.173:5000'
  },
  serverUrl: 'http://5cda228aeb39f80014a75091.mockapi.io/api',
  envName: 'DEV',
	version: '1.0.0'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
